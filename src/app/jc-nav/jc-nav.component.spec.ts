import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JcNavComponent } from './jc-nav.component';

describe('JcNavComponent', () => {
  let component: JcNavComponent;
  let fixture: ComponentFixture<JcNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JcNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JcNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
