import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogB04Component } from './blog-b04.component';

describe('BlogB04Component', () => {
  let component: BlogB04Component;
  let fixture: ComponentFixture<BlogB04Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogB04Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogB04Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
