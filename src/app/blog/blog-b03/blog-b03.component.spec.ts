import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogB03Component } from './blog-b03.component';

describe('BlogB03Component', () => {
  let component: BlogB03Component;
  let fixture: ComponentFixture<BlogB03Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogB03Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogB03Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
