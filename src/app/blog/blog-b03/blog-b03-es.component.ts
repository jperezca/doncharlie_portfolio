import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog-b03',
  templateUrl: './blog-b03-es.component.html',
  styleUrls: ['./blog-b03.component.css',
              '../blog-styles.css']
})
export class BlogB03ComponentEs implements OnInit {

  constructor() { }

  cmd_heroku_version = `  Microsoft Windows [Version 10.0.18362.476]
  (c) 2019 Microsoft Corporation. All rights reserved.
  
  C:\\Users\\don>heroku --version
   »   Warning: heroku update available from 7.24.1 to 7.26.2.
  heroku/7.24.1 win32-x64 node-v11.14.0
  
  C:\\Users\\don>`

  cmd_heroku_login = `  C:\\Users\\don>heroku login
    »   Warning: heroku update available from 7.24.1 to 7.35.0.
  heroku: Press any key to open up the browser to login or q to exit:
  Opening browser to https://cli-auth.heroku.com/auth/browser/e2cd3f38-8a72-4b6c-8f0c-81f8ac37768e
  Logging in... done
  Logged in as username@gmail.com`

  cmd_heroku_git_setup = `  C:\\Users\\don\\Documents\\Don Programmer\\first-app>git init
  Reinitialized existing Git repository in C:/Users/don/Documents/Don Programmer/first-app/.git/
  
  C:\\Users\\don\\Documents\\Don Programmer\\first-app>heroku git:remote -a don-programmer-code
  set git remote heroku to https://git.heroku.com/don-programmer-code.git
  
  C:\\Users\\don\\Documents\\Don Programmer\\first-app>`

  server_js_file = `  //Install express server
  const express = require('express');
  const path = require('path');
  const app = express();
  
  // Serve only the static files form the dist directory
  app.use(express.static('./dist/first-app'));
  app.get('/*', function(req,res) {
      
  res.sendFile(path.join(__dirname,'/dist/first-app/index.html'));
  });
  
  // Start the app by listening on the default Heroku port
  app.listen(process.env.PORT || 8080);`

  package_json_1 = `  {
    "name": "first-app",
    "version": "0.0.0",
    "scripts": {
      "ng": "ng",
      "start": "node server.js",`
  
  package_json_2 = `    "scripts": {
    "ng": "ng",
    "start": "node server.js",
    "build": "ng build",
    "test": "ng test",
    "lint": "ng lint",
    "e2e": "ng e2e",
    "postinstall": "ng build --aot --prod"
  },`

  package_json_3 = `    "engines": {
    "node": "10.15.3",
    "npm": "6.4.1"
  }`

  package_json_4 = `  "express": "^4.17.1"`

  package_json_5 = `  {
    "name": "first-app",
    "version": "0.0.0",
    "scripts": {
      "ng": "ng",
      "start": "node server.js",
      "build": "ng build",
      "test": "ng test",
      "lint": "ng lint",
      "e2e": "ng e2e",
      "postinstall": "ng build --aot --prod"
    },
    "private": true,
    "dependencies": {
      "@angular/animations": "~7.2.0",
      "@angular/common": "~7.2.0",
      "@angular/compiler": "~7.2.0",
      "@angular/core": "~7.2.0",
      "@angular/forms": "~7.2.0",
      "@angular/platform-browser": "~7.2.0",
      "@angular/platform-browser-dynamic": "~7.2.0",
      "@angular/router": "~7.2.0",
      "core-js": "^2.5.4",
      "rxjs": "~6.3.3",
      "tslib": "^1.9.0",
      "zone.js": "~0.8.26",
      "express": "^4.17.1"
    },
    "devDependencies": {
      "@angular-devkit/build-angular": "~0.13.0",
      "@angular/cli": "~7.3.7",
      "@angular/compiler-cli": "~7.2.0",
      "@angular/language-service": "~7.2.0",
      "@types/node": "~8.9.4",
      "@types/jasmine": "~2.8.8",
      "@types/jasminewd2": "~2.0.3",
      "codelyzer": "~4.5.0",
      "jasmine-core": "~2.99.1",
      "jasmine-spec-reporter": "~4.2.1",
      "karma": "~4.0.0",
      "karma-chrome-launcher": "~2.2.0",
      "karma-coverage-istanbul-reporter": "~2.0.1",
      "karma-jasmine": "~1.1.2",
      "karma-jasmine-html-reporter": "^0.2.2",
      "protractor": "~5.4.0",
      "ts-node": "~7.0.0",
      "tslint": "~5.11.0",
      "typescript": "~3.2.2"
    },
    "engines": {
      "node": "10.15.3",
      "npm": "6.4.1"
    }
  }`

  cmd_git_add_log = `  C:\\Users\\don\\Documents\\Don Programmer\\first-app>git add .
  warning: LF will be replaced by CRLF in .editorconfig.
  The file will have its original line endings in your working directory
  warning: LF will be replaced by CRLF in .gitignore.`

  cmd_git_status_log = `  C:\\Users\\don\\Documents\\Don Programmer\\first-app>git status
  On branch master
  
  No commits yet
  
  Changes to be committed:
    (use "git rm --cached <file>..." to unstage)
  
          new file:   .editorconfig
          new file:   .gitignore
          new file:   README.md
          new file:   angular.json
          new file:   e2e/protractor.conf.js
          new file:   e2e/src/app.e2e-spec.ts
          new file:   e2e/src/app.po.ts
          new file:   e2e/tsconfig.e2e.json
          new file:   package-lock.json
          new file:   package.json
          new file:   src/app/app-routing.module.ts
          new file:   src/app/app.component.css
          new file:   src/app/app.component.html
          new file:   src/app/app.component.spec.ts
          new file:   src/app/app.component.ts
          new file:   src/app/app.module.ts
          new file:   src/assets/.gitkeep
          new file:   src/browserslist
          new file:   src/environments/environment.prod.ts
          new file:   src/environments/environment.ts
          new file:   src/favicon.ico
          new file:   src/index.html
          new file:   src/karma.conf.js
          new file:   src/main.ts
          new file:   src/polyfills.ts
          new file:   src/styles.css
          new file:   src/test.ts
          new file:   src/tsconfig.app.json
          new file:   src/tsconfig.spec.json
          new file:   src/tslint.json
          new file:   tsconfig.json
          new file:   tslint.json`
  
  cmd_git_commit_log = `  C:\\Users\\don\\Documents\\Don Programmer\\first-app>git commit -m "Uploading app to Heroku"
  [master (root-commit) 38efdec] Uploading app to Heroku
   32 files changed, 11508 insertions(+)
   create mode 100644 .editorconfig
   create mode 100644 .gitignore
   create mode 100644 README.md
   create mode 100644 angular.json
   create mode 100644 e2e/protractor.conf.js
   create mode 100644 e2e/src/app.e2e-spec.ts
   create mode 100644 e2e/src/app.po.ts
   create mode 100644 e2e/tsconfig.e2e.json
   create mode 100644 package-lock.json
   create mode 100644 package.json
   create mode 100644 src/app/app-routing.module.ts
   create mode 100644 src/app/app.component.css
   create mode 100644 src/app/app.component.html
   create mode 100644 src/app/app.component.spec.ts
   create mode 100644 src/app/app.component.ts
   create mode 100644 src/app/app.module.ts
   create mode 100644 src/assets/.gitkeep
   create mode 100644 src/browserslist
   create mode 100644 src/environments/environment.prod.ts
   create mode 100644 src/environments/environment.ts
   create mode 100644 src/favicon.ico
   create mode 100644 src/index.html
   create mode 100644 src/karma.conf.js
   create mode 100644 src/main.ts
   create mode 100644 src/polyfills.ts
   create mode 100644 src/styles.css
   create mode 100644 src/test.ts
   create mode 100644 src/tsconfig.app.json
   create mode 100644 src/tsconfig.spec.json
   create mode 100644 src/tslint.json
   create mode 100644 tsconfig.json
   create mode 100644 tslint.json
  
  C:\\Users\\don\\Documents\\Don Programmer\\first-app>`

  cmd_git_push_log = `  C:\\Users\\don\\Documents\\Don Programmer\\first-app>git push heroku master
  Enumerating objects: 39, done.
  Counting objects: 100% (39/39), done.
  Delta compression using up to 8 threads
  Compressing objects: 100% (37/37), done.
  Writing objects: 100% (39/39), 107.22 KiB | 1.26 MiB/s, done.
  Total 39 (delta 0), reused 0 (delta 0)
  remote: Compressing source files... done.
  remote: Building source:
  remote:
  remote: -----> Node.js app detected
  remote:
  remote: -----> Creating runtime environment
  remote:
  remote:        NPM_CONFIG_LOGLEVEL=error
  remote:        NODE_ENV=production
  remote:        NODE_MODULES_CACHE=true
  remote:        NODE_VERBOSE=false
  remote:
  remote: -----> Installing binaries
  ..............`

  cmd_git_push_success = `  remote: -----> Build succeeded!
  remote: -----> Discovering process types
  remote:        Procfile declares types     -> (none)
  remote:        Default types for buildpack -> web
  remote:
  remote: -----> Compressing...
  remote:        Done: 40.6M
  remote: -----> Launching...
  remote:        Released v3
  remote:        https://don-programmer-code.herokuapp.com/ deployed to Heroku
  remote:
  remote: Verifying deploy... done.
  To https://git.heroku.com/don-programmer-code.git
   * [new branch]      master -> master
  
  C:\\Users\\don\\Documents\\Don Programmer\\first-app>`


  ngOnInit() {
  }

}
