import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogB06Component } from './blog-b06.component';

describe('BlogB06Component', () => {
  let component: BlogB06Component;
  let fixture: ComponentFixture<BlogB06Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogB06Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogB06Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
