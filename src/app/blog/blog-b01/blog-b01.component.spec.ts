import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogB01Component } from './blog-b01.component';

describe('BlogB01Component', () => {
  let component: BlogB01Component;
  let fixture: ComponentFixture<BlogB01Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogB01Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogB01Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
