import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogB05Component } from './blog-b05.component';

describe('BlogB05Component', () => {
  let component: BlogB05Component;
  let fixture: ComponentFixture<BlogB05Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogB05Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogB05Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
