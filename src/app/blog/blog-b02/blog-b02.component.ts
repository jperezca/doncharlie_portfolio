import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog-b02',
  templateUrl: './blog-b02.component.html',
  styleUrls: ['./blog-b02.component.css']
})
export class BlogB02Component implements OnInit {

  constructor() { }

  texto_code = `  import { Component } from '@angular/core';
    
  @Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
  })
  export class AppComponent {
    title = 'Don Charlie website!';
  }`

  texto_code_2 = `  C:\\Users\\don\\Documents\\don>cd first-app
  
  C:\\Users\\don\\Documents\\don\\first-app>ng serve --open
  ** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **
  
  Date: 2019-11-14T23:40:14.881Z
  Hash: badd0f7a542bd5f078c9
  Time: 16147ms
  chunk {es2015-polyfills} es2015-polyfills.js, es2015-polyfills.js.map (es2015-polyfills) 285 kB [initial] [rendered]
  chunk {main} main.js, main.js.map (main) 11.6 kB [initial] [rendered]
  chunk {polyfills} polyfills.js, polyfills.js.map (polyfills) 236 kB [initial] [rendered]
  chunk {runtime} runtime.js, runtime.js.map (runtime) 6.08 kB [entry] [rendered]
  chunk {styles} styles.js, styles.js.map (styles) 16.3 kB [initial] [rendered]
  chunk {vendor} vendor.js, vendor.js.map (vendor) 3.77 MB [initial] [rendered]
  i ｢wdm｣: Compiled successfully.`

  ngOnInit() {
  }

}
