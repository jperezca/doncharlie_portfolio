import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogB02Component } from './blog-b02.component';

describe('BlogB02Component', () => {
  let component: BlogB02Component;
  let fixture: ComponentFixture<BlogB02Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogB02Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogB02Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
