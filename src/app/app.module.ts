import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule, ExtraOptions } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { ContactMeComponent } from './contact-me/contact-me.component';
import { BlogB01Component } from './blog/blog-b01/blog-b01.component';
import { BlogB02Component } from './blog/blog-b02/blog-b02.component';
import { BlogB02ComponentEs } from './blog/blog-b02/blog-b02-es.component';
import { BlogB03Component } from './blog/blog-b03/blog-b03.component';
import { BlogB03ComponentEs } from './blog/blog-b03/blog-b03-es.component';
import { BlogB04Component } from './blog/blog-b04/blog-b04.component';
import { BlogB05Component } from './blog/blog-b05/blog-b05.component';
import { BlogB06Component } from './blog/blog-b06/blog-b06.component';
import { JcNavComponent } from './jc-nav/jc-nav.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { BlogComponent } from './blog/blog.component';
import { ResumeComponent } from './resume/resume.component';
import { BlogNotFoundComponent } from './blog/blog-not-found/blog-not-found.component';

var appRoutes = [
  {
    pathMatch: 'full',
    path: '',
    redirectTo: '/home'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'blog',
    component: BlogComponent
  },
  {
    path: 'blog-who-am-i',
    component: BlogB01Component
  },
  {
    path: 'blog-starting-with-angular',
    component: BlogB02Component
  },
  {
    path: 'blog-comenzando-con-angular',
    component: BlogB02ComponentEs
  },
  {
    path: 'blog-upload-website-heroku',
    component: BlogB03Component
  },
  {
    path: 'blog-subir-website-heroku',
    component: BlogB03ComponentEs
  },
  {
    path: 'blog-my-code-website',
    component: BlogB04Component
  },
  {
    path: 'blog-not-found',
    component: BlogNotFoundComponent
  },
  {
    path: 'resume',
    component: ResumeComponent
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: '/404'
  }
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    ContactMeComponent,
    BlogB01Component,
    BlogB02Component,
    BlogB02ComponentEs,
    BlogB03Component,
    BlogB03ComponentEs,
    BlogB04Component,
    BlogB05Component,
    BlogB06Component,
    JcNavComponent,
    NotFoundComponent,
    BlogComponent,
    ResumeComponent,
    BlogNotFoundComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes, { onSameUrlNavigation: 'reload'} ),
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
